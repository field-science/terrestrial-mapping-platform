#!/usr/bin/env python3

# fetch-gdrive-object.py 
'''command-line file tranfer from Google Drive without authentication'''

# Basic instructions:
# 1) Make sheet, doc, etc. viewable by anyone with the URL.
# 2) Extract the document ID from the browser's address bar (it is between the .../d/ and the next /)
# 3) fetch-gdrive-object -d document-id -t object-type -f output-format -o output-filename

# The available file formats vary by object type (you must provide a reasonable combination).
# 	Google Docs: .docx, .odt, .rtf, .pdf, .txt, .html, and .epub
# 	Google Sheets: .xslx, .ods, .csv, .tsv, .pdf, and .html
# 	Google Slides: .pptx, .pdf, .svg, .jpg, .png, and .txt
# 	Google Drawings: pdf, .svg, .jpg, .png
# 	Files: 

# todo 
#	test on all machines (py2 or py3 code?)
#	look for -i error in log to determine success? (in fetchObject())
# 	support for other object types: 
#		file - done
#		presentation 
#		drawings # slide is the language used elsewhere 
#		text 
#	pylint 
#	test under osx so that people can use it on their local machines to fetch workflows 

import sys
import os

def fetchObject(documentId, objectType, outputFormat, outputName, logFile):
	base = 'https://docs.google.com/'

	if objectType == 'sheet':
		location = 'spreadsheets/d/'

	elif objectType == 'doc':
		location = 'document/d/'

	elif objectType == 'file':
		location = 'file/d/'

	# DOC_ID=1olFTlH4ssJIv01n40-44BIDda0OkXcFhXj-SLVtZwks

	command = 'wget --output-file=' + logFile + ' \"' + base + location + documentId + '/export?format=' + outputFormat 

	if objectType == 'sheet':
		command = command + '&gid=0\" '
	else:
		command = command + '\" '

	command = command + '-O ' + outputName

	_status = os.system(command) 

	return _status

if __name__ == "__main__":
	import argparse

	parser = argparse.ArgumentParser(description='fetch-gdrive-object')
	parser.add_argument('-d','--documentId', type=str, help='GDrive Document ID', required=True)
	parser.add_argument('-t','--objectType', type=str, choices=['doc', 'sheet', 'file'], help='Object type {doc, sheet, file}', required=True)
	parser.add_argument('-f','--outputFormat', type=str, help='Output format (must fit type)', required=True)
	parser.add_argument('-o','--outputName', type=str, help='Output file name', required=True)
	args = parser.parse_args()

	logFile = 'fetch-gdrive-object.log'

	# todo - support for presentation and drawings 
	# todo - support for file type of object

	if args.objectType == 'doc' and args.outputFormat in ['docx', 'odt', 'rtf', 'pdf', 'txt', 'html', 'epub']:
		status = fetchObject(args.documentId, args.objectType, args.outputFormat, args.outputName, logFile)

	elif args.objectType == 'sheet' and args.outputFormat in ['xslx', 'ods', 'csv', 'tsv', 'pdf', 'html']:
		status = fetchObject(args.documentId, args.objectType, args.outputFormat, args.outputName, logFile)

	elif args.objectType == 'file' and args.outputFormat in ['xslx', 'ods', 'csv', 'tsv', 'pdf', 'html', 'jpg', 'png']:
		status = fetchObject(args.documentId, args.objectType, args.outputFormat, args.outputName, logFile)

	else:
		print('fetch-gdrive-object: error, unsupported objectType or objectType outputFormat combination')
		exit(-1)

	if status == 0:
		exit(0)

	elif status == 2048:
		print('fetch-gdrive-object: error, file ID of that type not found', file=sys.stderr)
		exit(-1)
	
	else:
		print('fetch-gdrive-object: error, code:', status, ', see', logFile, 'for more information.', file=sys.stderr)
		exit(-1)