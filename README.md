# The Terrestrial Mapping Platform

Simplifying the collection and assembly of UAV images into 2D and 3D assemblies for use by a wide variety of domain scientists. 


# Table of Contents
* Technical design documents
* Software workflows
* Documentation
* Usage guidelines
* Support/issue tracking 


# Documentation

For documentation including usage guidelines and checklists, please see the [wiki](https://code.cs.earlham.edu/field-science/terrestrial-mapping-platform/-/wikis/home).

# Related Projects

Many TMP projects have dedicated repositories. These include:

* [UAV applications](https://code.cs.earlham.edu/field-science/uav_app)
* [Image analysis workflows](https://code.cs.earlham.edu/field-science/uav_app)

# Issues

For problems using or building the TMP generally, please submit an issue through the [Issues tab](https://code.cs.earlham.edu/field-science/terrestrial-mapping-platform/-/issues) here. Problems specific to a related project with its own repository should be submitted to the Issues tracker there.